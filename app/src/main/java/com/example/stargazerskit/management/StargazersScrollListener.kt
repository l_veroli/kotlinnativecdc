package com.example.stargazerskit.management

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager


abstract class StargazersScrollListener(layoutManager: LinearLayoutManager) :
RecyclerView.OnScrollListener() {

    private val mLayoutManager = layoutManager

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = mLayoutManager.childCount
        val totalItemCount = mLayoutManager.itemCount
        val firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition()

        if(firstVisibleItemPosition > 0){
            showScrollUpButton()
        }else{
            hideScrollUpButton()
        }

        if (!isLoading() && !isLastPage()) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0) {
                loadMoreStargazers()
            }
        }
    }

    abstract fun loadMoreStargazers()
    abstract fun isLastPage(): Boolean
    abstract fun isLoading(): Boolean
    abstract fun showScrollUpButton()
    abstract fun hideScrollUpButton()
}