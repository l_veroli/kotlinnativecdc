package com.example.stargazerskit.networking

import com.example.stargazerskit.management.isNotNullAndNotEmpty
import com.example.stargazerskit.management.model.Stargazer
import com.example.stargazerskit.networking.model.NetworkStargazer

object ResponseMapper {

    fun stargazerMapper(stargazersList: ArrayList<NetworkStargazer>): ArrayList<Stargazer> {
        return stargazersList.filter { item -> item.login.isNotNullAndNotEmpty() && item.avatarUrl.isNotNullAndNotEmpty() }
            .map { item -> Stargazer(item.login!!, item.avatarUrl!!) }
            .toCollection(ArrayList())
    }
}