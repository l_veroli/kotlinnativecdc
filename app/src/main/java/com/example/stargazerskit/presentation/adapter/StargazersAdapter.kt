package com.example.stargazerskit.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.stargazerskit.R
import com.example.stargazerskit.management.model.Stargazer
import com.facebook.shimmer.ShimmerFrameLayout

class StargazersAdapter(
    context: Context?,
    shimmerElement: Int,
    stargazerListener: AdapterInteractionListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    enum class HolderType {
        SHIMMER_TYPE,
        STARGAZER,
        ERROR_TYPE
    }

    private val mContext = context
    private val STARGAZER_HOLDER = 1
    private val SHIMMER_HOLDER = 2
    private val ERROR_HOLDER = 3
    private val mListener = stargazerListener
    private var mItems: ArrayList<RowWrapper<*>> = ArrayList()

    private data class RowWrapper<T>(val element: T, val type: HolderType)

    init {
        startShimmer(shimmerElement)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(mContext)
        val view: View
        return when (viewType) {
            STARGAZER_HOLDER -> {
                view = inflater.inflate(R.layout.layout_stargazer_item, parent, false)
                StargazerViewHolder(view)
            }
            SHIMMER_HOLDER -> {
                view = inflater.inflate(R.layout.layout_stargazer_shimmer_item, parent, false)
                ShimmerViewHolder(view)
            }

            ERROR_HOLDER -> {
                view = inflater.inflate(R.layout.layout_network_error, parent, false)
                ErrorViewHolder(view)
            }

            else -> {
                view = inflater.inflate(R.layout.layout_empty_item, parent, false)
                EmptyViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mItems[position].type) {
            HolderType.STARGAZER -> STARGAZER_HOLDER
            HolderType.SHIMMER_TYPE -> SHIMMER_HOLDER
            HolderType.ERROR_TYPE -> ERROR_HOLDER
        }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ShimmerViewHolder -> {
                holder.shimmerLayout.startShimmer()
            }

            is StargazerViewHolder -> {
                val stargazer = mItems[position].element as Stargazer
                holder.ownerName.text = stargazer.username
                holder.itemView.setOnClickListener {
                    mListener.onStargazerClicked(mItems[position].element as Stargazer)
                }

                mContext?.let { context ->
                    Glide.with(context)
                        .load(stargazer.avatarUrl)
                        .circleCrop()
                        .placeholder(R.drawable.avatar_placeholder)
                        .into(holder.avatarIcon)
                }
            }

            is ErrorViewHolder -> {
                val message = mItems[position].element as String
                holder.errorLabel.text = message
            }
        }
    }

    private fun startShimmer(shimmerElement: Int) {
        for (index in 0..shimmerElement) {
            mItems.add(RowWrapper(null, type = HolderType.SHIMMER_TYPE))
        }

        notifyItemRangeChanged(0, 5)
    }

    fun setData(stargazersList: ArrayList<Stargazer>) {
        val originalSize = mItems.size

        removeShimmerElements()

        mItems.addAll(stargazersList.map { item -> RowWrapper(item, HolderType.STARGAZER) })
        mItems.add(RowWrapper(null, HolderType.SHIMMER_TYPE))

        notifyItemRangeChanged(originalSize, mItems.size)
    }

    fun removeLoaderElement() {
        if (mItems[mItems.lastIndex].type == HolderType.SHIMMER_TYPE) {
            mItems.removeAt(mItems.lastIndex)
            notifyItemRemoved(mItems.lastIndex)
        }
    }

    fun addErrorMessage(message: String) {
        removeShimmerElements()
        mItems.add(RowWrapper(message, HolderType.ERROR_TYPE))
        notifyDataSetChanged()
    }

    private fun removeShimmerElements() {
        mItems.removeAll { item ->
            item.type == HolderType.SHIMMER_TYPE
        }
    }

     class StargazerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val avatarIcon = view.findViewById<ImageView>(R.id.avatarIcon)
        val ownerName = view.findViewById<TextView>(R.id.ownerName)
    }

     class ShimmerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val shimmerLayout = view.findViewById<ShimmerFrameLayout>(R.id.shimmer_layout)
    }

     class EmptyViewHolder(view: View) : RecyclerView.ViewHolder(view)

     class ErrorViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val errorLabel = view.findViewById<TextView>(R.id.networkUnreachabilityMessage)
    }


    interface AdapterInteractionListener {
        fun onStargazerClicked(stargazer: Stargazer)
    }
}