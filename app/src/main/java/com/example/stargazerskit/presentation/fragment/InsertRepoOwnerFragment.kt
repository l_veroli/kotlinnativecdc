package com.example.stargazerskit.presentation.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import com.example.stargazerskit.R
import com.example.stargazerskit.management.isNotNullAndNotEmpty
import kotlinx.android.synthetic.main.layout_insert_repo_owner_fragment.*

class InsertRepoOwnerFragment : Fragment() {

    private var mInteractionListener: InsertRepoOwnerListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is InsertRepoOwnerListener) {
            mInteractionListener = context
        } else {
            AlertDialog.Builder(context)
                .setTitle("Attenzione")
                .setMessage("Implentare l'interfaccia InsertRepoOwnerListener")
                .setNeutralButton("OK") { dialog, _ ->
                    dialog.dismiss()
                    activity?.onBackPressed()
                }
                .show()
        }
    }

    override fun onDetach() {
        mInteractionListener = null
        super.onDetach()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_insert_repo_owner_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mInteractionListener?.refreshToolbar(getString(R.string.stargazers_title))

        ownerEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateFields()
            }
            true
        }


        searchButton.setOnClickListener {
            validateFields()
        }
    }

    private fun validateFields() {
        if (repoEditText.text.toString().isNotNullAndNotEmpty() &&
            ownerEditText.text.toString().isNotNullAndNotEmpty()
        ) {
            val direction = InsertRepoOwnerFragmentDirections.navigateToStargazersList(
                repoEditText.text.toString(),
                ownerEditText.text.toString()
            )

            hideKeyboard(repoEditText)
            mInteractionListener?.onDataInserted(direction)
        } else {
            AlertDialog.Builder(context)
                .setTitle(getString(R.string.attention))
                .setMessage(getString(R.string.missing_parameter_error))
                .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }


    interface InsertRepoOwnerListener {
        fun refreshToolbar(title: String, shouldShowBackButton: Boolean = false)
        fun onDataInserted(directions: NavDirections)
    }
}