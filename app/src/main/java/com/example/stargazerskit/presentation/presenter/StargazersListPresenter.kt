package com.example.stargazerskit.presentation.presenter

import android.content.Context
import android.net.ConnectivityManager
import com.example.stargazerskit.management.isNotNullAndNotEmpty
import com.example.stargazerskit.networking.ResponseMapper
import com.example.stargazerskit.networking.repository.Repository
import com.example.stargazerskit.presentation.contract.StargazersListContract
import kotlinx.coroutines.*


class StargazersListPresenter(view: StargazersListContract.MainView) : StargazersListContract.MainPresenter {

    private val mView = view
    private var deferredJob : Job? = null


    override fun getRepositoryStargazersList(repository: String?, repositoryOwner: String?, pageNumber: Int) {
        if(isPhoneConnected()) {
            if(repository.isNotNullAndNotEmpty() && repositoryOwner.isNotNullAndNotEmpty()) {
                deferredJob = CoroutineScope(Dispatchers.IO).launch {
                    val repoList = Repository.retrieveRepositoryStargazerList(
                        repository!!,
                        repositoryOwner!!,
                        pageNumber
                    )

                    withContext(Dispatchers.Main) {
                        repoList?.let { list ->
                            val convertedList = ResponseMapper.stargazerMapper(list)
                            if(pageNumber > 1){
                                mView.onNextStargazersPageRetrieved(convertedList)
                            }else{
                                mView.onFirstStargazersListRetrieved(convertedList)
                            }
                        } ?: mView.onRetrievingStargazersListError()
                    }
                }
            }else{
                mView.onRetrievingStargazersListError()
            }
        }else{
            mView.onPhoneNotConnected()
        }
    }

    private fun isPhoneConnected(): Boolean{
        val connectivityManager = mView.getContext()?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!.isConnected
    }

    override fun cancelJob() {
        deferredJob?.cancel()
    }
}