package com.example.stargazerskit.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.*
import com.example.stargazerskit.R
import com.example.stargazerskit.presentation.fragment.InsertRepoOwnerFragment
import com.example.stargazerskit.presentation.fragment.StargazersListFragment
import com.example.stargazerskit.presentation.presenter.StargazersListPresenter
import kotlinx.android.synthetic.main.layout_stargazers_main_activity.*

class StargazersMainActivity : AppCompatActivity(),
    InsertRepoOwnerFragment.InsertRepoOwnerListener,
    StargazersListFragment.StargazersListInterface {

    private var mPresenter: StargazersListPresenter? = null
    private val mNavController: NavController by lazy {
        findNavController(R.id.frameContainer)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_stargazers_main_activity)
    }

    override fun onDestroy() {
        mPresenter?.cancelJob()
        super.onDestroy()
    }

    override fun onDataInserted(directions: NavDirections) {
        navigateToDestination(directions)
    }

    override fun refreshToolbar(title: String, shouldShowBackButton: Boolean) {
        toolbarTitle.text = title
        if (shouldShowBackButton) {
            backButton.visibility = View.VISIBLE
            backButton.setOnClickListener {
                mNavController.popBackStack()
            }
        } else {
            backButton.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        if (!mNavController.popBackStack()) {
            finish()
        }
    }

    override fun openStargazerDetail(direction: NavDirections) {
        navigateToDestination(direction)
    }

    private fun navigateToDestination(destination: NavDirections) {
        mNavController.navigate(destination)
    }
}
